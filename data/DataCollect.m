% status = 'ERROR';
% s = serial('COM8','Terminator','CR');
% fopen(s);
% while strcmp(status,'ERROR')
% fprintf(s,'S0060');
% status = fscanf(s)
% end
% fclose(s);
% rec_action = {'uwalk','unrun','awalk','arrun','gwalk','gurun','pullg','answe',
    rec_action = {'boxwa','boxst','aimwa','crawl'};

action_counter = 1;
FrameSize = 2048;
% AR = dsp.AudioFileReader('Filename','First Test.wav',...
%     'SamplesPerFrame',FrameSize);
AR = dsp.AudioRecorder('DeviceName', 'Line (2- BGT24 Referenzdesign)',...
    'NumChannels', 4,...
    'SamplesPerFrame',FrameSize);
Fs = AR.SampleRate;

AP = dsp.AudioPlayer('SampleRate',Fs,...
    'OutputNumUnderrunSamples',true);
%Constants
WaveLength = 3E8/24E9;
MaxVel = Fs/2*WaveLength;

% filter
% Wo = 200/(Fs/2);
% Q  = 35;
% BW = Wo/Q;
% [b,a] = iirnotch(Wo,BW);
% NotchFilter = dsp.BiquadFilter('SOSMatrix',[b,a]);

% stream
% TS = dsp.SpectrumAnalyzer('YLimits',[-1,1],'SampleRate',Fs,...
%     'TimeSpan',FrameSize/Fs);
% step(TS,audioOut);
tic
dropped = 0;
count = 0;
TimeWindowSize = 64;
TWSdiv = 16;
% PhaseFall = zeros(FrameSize,TimeWindowSize/TWSdiv);
Name = input('Type the name: ','s');
fh1 = figure(1);
set(fh1,'Name',Name)
clf
paus = uicontrol('style','togglebutton','parent',fh1,'string',rec_action{2});

ax1 = axes('parent',fh1);
% ax2 = subplot(3,1,2,'parent',fh1);
% ax3 = subplot(3,1,3,'parent',fh1);
freqV = linspace(-MaxVel,MaxVel,FrameSize+1);
freqV = freqV(1:end-1);
freqVec = freqV(abs(freqV)<10);
timeVec = linspace(0,TimeWindowSize*FrameSize/Fs,TimeWindowSize+1);
timeVec = timeVec(2:end);

WaterFall = zeros(length(freqVec),TimeWindowSize); % freq x time

IM = imagesc(timeVec,freqVec,WaterFall,'parent',ax1);
set(ax1,'yDir','normal')
xlabel('time (s)')
ylabel('velocity (m/s)');
% 
% IM2 = imagesc(timeVec,freqVec,WaterFall,'parent',ax2);
% xlabel('time (s)')
% ylabel('velocity (m/s)');
% 
% IM3 = imagesc(timeVec,freqVec,WaterFall,'parent',ax3);
% xlabel('time (s)')
% ylabel('velocity (m/s)');

wFun = hanning(FrameSize);%
recordAudio = [];
recMat = [];
while 1==1%toc < Tstop
    audioIn = step(AR);
    audioOut = audioIn(:,1) + 1i*audioIn(:,2);% sum(audioIn,2);
    audioOut2 = audioIn(:,3) + 1i*audioIn(:,4);
    recordAudio = [recordAudio;[sum(audioIn(:,[1,3]),2) diff(audioIn(:,[2,4]),1,2)]];
    recMat = [recMat;[audioOut audioOut2]];
    WaterFall(:,2:end) = WaterFall(:,1:end-1);
    temp = fftshift(fft( (audioOut+conj(audioOut2)).*wFun ));
    WaterFall(:,1) = 20*log10(abs(temp(abs(freqV)<10)));
 
    
%     WaterFall2(:,2:end) = WaterFall2(:,1:end-1);
%     temp2 = fftshift(fft( conj(audioOut2).*hann ));
%     WaterFall2(:,1) = 20*log10(abs(temp2(1:end)));
%     
%     WaterFall3(:,2:end) = WaterFall3(:,1:end-1);
%     temp3 = (temp2./temp);
%     WaterFall3(:,1) = 20*log10(abs(temp3(1:end)));
%     audioOut = step(NotchFilter,audioIn);
    IM.CData = WaterFall;
    drawnow
%     IM2.CData = WaterFall2;
%     drawnow
    count = count + 1;
    if count*256 >4096;
        count = 0;
    end
%     dropped = dropped + step(AP,abs(audioOut));
% fprintf(s,'0%s',dec2hex(count*256,4));
% %     fprintf(s,'1%s',dec2hex(count*256,4));
% %     fprintf('1%s\n',dec2hex(count*256,4));
% %     p = fscanf(s)
   	if paus.Value
%         fscanf(s)
        fileName = [rec_action{action_counter} '_' Name(1:5)];
%         title(ax1,rec_action{action_counter})
        audiowrite([fileName '.wav'],recordAudio,Fs);
        save([fileName '.mat'],'recMat');
        
        recordAudio = [];
        recMat = [];
%         break

        input(rec_action{action_counter},'s');
        action_counter = action_counter + 1;
        paus.Value = 0;
    if action_counter == length(rec_action);
        paus.String = 'STOP';
    elseif action_counter > length(rec_action); 
        close(fh1)
        break;
    else        
%         title(ax1,rec_action{action_counter-1})
        paus.String = rec_action{action_counter+1};
    end
    end
end
% audiowrite('recording.wav',recordAudio,Fs);
% fclose(s);
% delete(s);