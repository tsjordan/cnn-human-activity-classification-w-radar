def getLabel(line):
    lab = line[0:5]
    if lab == 'uwalk':
        label = 0
    elif lab == 'unrun':
        label = 1
    elif lab == 'boxst':
        label = 2
    elif lab == 'boxwa':
        label = 3
    elif lab == 'aimwa':
        label = 4
    elif lab == 'crawl':
        label = 5
    else:
        label =-1
    return label

def valid(line):
    lab = line[6:11]
    if lab == 'alpha':
        return 0
    elif lab == 'bravo':
        return 0

    elif lab == 'charl':
        return 0
    elif lab == 'delta':
        return 0

    elif lab == 'echoe':
        return 1
    elif lab == 'foxtr':
        return 1

    elif lab == 'gamma':
        return 2
    elif lab == 'hotel':
        return 2
    elif lab == 'india':
        return 2
    else:
        return -1

f = open('list.txt')
g = open('train.txt','w')
v = open('valid.txt','w')
t = open('testt.txt','w')
for line in f:
    label = getLabel(line)
    if label >= 0:
        if valid(line)==1:
            v.write(line[0:-1]+' '+str(label)+'\n')
        elif valid(line)==0:
            g.write(line[0:-1]+' '+str(label)+'\n')
        elif valid(line)==2:
            t.write(line[0:-1]+' '+str(label)+'\n')

f.close()
g.close()
v.close()
t.close()



