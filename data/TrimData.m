[FileName,PathName,FilterIndex] = uigetfile('.mat','multiSelect','on');
if ischar(FileName)
    temp = FileName;
    clear FileName;
    FileName{1} = temp;
end
fs = 44.1e3;

numFiles = length(FileName);
for n = 1:numFiles
    load([PathName FileName{n}])
    if ~exist('ea')        
        disp(FileName{n})
        Yfilt = recMat(:,1) + conj(recMat(:,2));
        [s,f,t]=spectrogram(Yfilt,4096,[],[],fs,'centered','yaxis');
        fh = figure(1);
        clf
        ax = axes('parent',fh);
        imagesc(t,f(end/2-92:end/2+92),log(abs(s(end/2-92:end/2+92,:))),'parent',ax);
        ylabel('frequency (Hz)')
        xlabel('time (s)')
        datacursormode on
        
        sa = input('Start Walking Away: ');
        ea = input('End   Walking Away: ');
        st = input('Start Walking Towards: ');
        et = input('End   Walding Towards: ');
        fprintf('\n')
        %     A = recMat(sa*fs:ea*fs,:);
        %     T = recMat(st*fs:et_fs,:);
        
        save([PathName FileName{n}],'sa','ea','st','et','-append')
    end
    clear recMat sa ea st et
    
end

