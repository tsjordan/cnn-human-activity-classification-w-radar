[FileName,PathName,FilterIndex] = uigetfile('.mat','multiSelect','on');
if ischar(FileName)
    temp = FileName;
    clear FileName;
    FileName{1} = temp;
end
nfft = 1024;
fs = 44.1e3;
clipLength = 65536+nfft/2;
overlap = 16384;

% filter
hpFilt = designfilt('highpassiir','FilterOrder',5, ...
    'PassbandFrequency',120,'PassbandRipple',2, ...
    'SampleRate',44.1e3);

sz = 128;
numData = 1590;
numFiles = length(FileName);
count = 0;
for n = 1:numFiles
    load([PathName FileName{n}])
    if exist('ea','var')
        if ea-sa ~= 0
            A = recMat(round(sa*fs):round(ea*fs) , :);
            
            %         T = recMat(round(st*fs):round(et*fs) , :);
            index = 1;
            while index+clipLength < size(A,1)
                temp = A(index:index + clipLength-1,:);
                Y = temp(:,1) + conj(temp(:,2));
                Yfilt = filter(hpFilt,Y);
                
                [s,f,t]=spectrogram(Yfilt,nfft,nfft/2,nfft,fs,'centered','yaxis');
                S  = log(abs(s(end/2-63:end/2+64,:)));
                scaled = (S-mean(S(:)));
                scaled = scaled/std(scaled(:));
                count = count + 1;
                imsave
                switch FileName{n}(1:5)
                    case 'uwalk'
                        LABL(count) = 0;
                    case 'unrun'
                        LABL(count) = 1;
                    case 'awalk'
                        LABL(count) = 2;
                    case 'arrun'
                        LABL(count) = 3;
                    case 'gwalk'
                        LABL(count) = 4;
                    case 'gurun'
                        LABL(count) = 5;
                    case 'pullg'
                        LABL(count) = 6;
                    case 'answe'
                        LABL(count) = 7;
                end
                
                
                %             F = f(end/2-63:end/2+64);
                %             fh = figure(1);
                %             clf
                %             ax = axes('parent',fh);
                %             imagesc(t,F,S,'parent',ax);
                %             ylabel('frequency (Hz)')
                %             xlabel('time (s)')
                %             input(' ')
                            index = index + overlap;
            end
        end
        
        
    end
end